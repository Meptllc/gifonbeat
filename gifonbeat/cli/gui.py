import click
from datetime import datetime
import imageio
import io
import PySimpleGUI as sg
import os
from PIL import Image, ImageTk, UnidentifiedImageError
from audioread.exceptions import NoBackendError

import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import librosa
import sys

from .accelerate import accelerate

@click.command()
@click.pass_context
def gui(ctx):
    """GUI to manage the gifonbeat process."""
    window = gui_window()
    gif_io = None
    audio_io = None
    song_bpm = 0
    multiplier = 1.0
    figure_canvas_agg = setup_matplotlib(window['-CANVAS-'].TKCanvas)

    # Event Loop to process "events" and get the "values" of the inputs
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Exit':
            break
        elif event == '-GIFFILENAME-' and values[event]:
            try:
                gif_io = Image.open(values[event])
            except UnidentifiedImageError:
                gif_io = None

            if gif_io:
                window['-GIF-'].update(data=into_file_bytes(gif_io))
                window['-GIFSLIDER-'].update(range=(0, gif_io.n_frames - 1), value=0)
            window['-GIFCONTROLS-'].update(visible=gif_io is not None)
        elif event == '-GIFSLIDER-' and gif_io:
            gif_io.seek(int(values[event]))
            window['-GIF-'].update(data=into_file_bytes(gif_io))
        elif event == '-MARKBEAT-':
            frame_number = int(values['-GIFSLIDER-'])
            if frame_number != 0:
                estimated_bpm = estimate_gif_bpm(values['-GIFFILENAME-'], frame_number)
                window['-GIFBPM-'].update(estimated_bpm)
        elif event == '-SONGFILENAME-' and values[event]:
            loaded = load_song(values, window['-SONGBPM-'], figure_canvas_agg)
            if loaded:
                song_bpm = int(window['-SONGBPM-'].DisplayText)
            window['-SONGCONTROLS-'].update(visible=loaded)
        elif event == '-DIVIDE2-':
            multiplier *= 0.5
            window['-SONGBPM-'].update(int(song_bpm * multiplier))
        elif event == '-TIMES2-':
            multiplier *= 2.0
            window['-SONGBPM-'].update(int(song_bpm * multiplier))
        elif event == '-STARTTIME-' or event == '-ENDTIME-':
            start_time = time_str_to_seconds(values['-STARTTIME-'])
            end_time = time_str_to_seconds(values['-ENDTIME-'])
            if start_time and end_time and start_time < end_time:
                update_plot_verticals(values)
                figure_canvas_agg.draw()
        elif event == '-PLAYPAUSE-':
            if window[event].get_text() == 'Play':
                # Intention is to play.
                start_time = time_str_to_seconds(values['-STARTTIME-'])
                end_time = time_str_to_seconds(values['-ENDTIME-'])
                window[event].update('Pause')
            else:
                window[event].update('Play')
            window['-PLAYPAUSE-']
        elif event == '-EXPORTFILENAME-' and values[event]:
            # Check on a bunch of dependencies of the export button.
            start_time = time_str_to_seconds(values['-STARTTIME-'])
            end_time = time_str_to_seconds(values['-ENDTIME-'])
            try:
                # Not sure why int() returns a tuple here.
                gif_bpm = int(window['-GIFBPM-'].DisplayText)
            except ValueError:
                gif_bpm = None

            # Ensure every dependent argument is filled. It would be preferred
            # to have the UI force these dependencies but we're not there yet.
            if (values['-GIFFILENAME-'] and values['-SONGFILENAME-'] and
                start_time and end_time and start_time < end_time and
                gif_bpm and song_bpm):
                ctx.invoke(accelerate,
                           gif_path=values['-GIFFILENAME-'],
                           gif_bpm=gif_bpm,
                           audio_path=values['-SONGFILENAME-'],
                           audio_bpm=song_bpm * multiplier,
                           output_file=values['-EXPORTFILENAME-'],
                           audio_start=start_time,
                           audio_duration=end_time - start_time)
            else:
                print(values['-GIFFILENAME-'])
                print(values['-SONGFILENAME-'])
                print(values['-EXPORTFILENAME-'])
                print(gif_bpm)
                print(song_bpm * multiplier)
                print(start_time)
                print(end_time)
    window.close()

def gui_window():
    image_preview_column = [
        [
            sg.Input(key='-GIFFILENAME-', visible=False, enable_events=True),
            sg.FileBrowse(file_types=(("GIF Files", "*.gif"), ('All Files', '*.*')))
        ],
        [sg.Image(key='-GIF-', size=(500, 500))],
        [sg.Frame('', [[
            sg.Slider(key='-GIFSLIDER-', range=(0, 0), orientation='horizontal', disable_number_display=True, enable_events=True),
            sg.Button('Mark Beat', key='-MARKBEAT-'),
            sg.Text('Estimated BPM: '),
            sg.Text(' ' * 5, key='-GIFBPM-', auto_size_text=True),
        ]], key='-GIFCONTROLS-', visible=False)]
    ]
    song_preview_column = [
        [
            sg.FileBrowse(file_types=(('Audio Files', '*.wav *.ogg *.mp3 *.flac'), ('All Files', '*.*'))),
            sg.Input(key='-SONGFILENAME-', visible=False, enable_events=True)
        ],
        [sg.Canvas(key='-CANVAS-')],
        [sg.Frame('', [
            [
                sg.Text('Start time: '), sg.Input('00:00:00.000', key='-STARTTIME-', enable_events=True),
                sg.Text('End time: '), sg.Input('00:00:30.000', key='-ENDTIME-', enable_events=True)
            ],
            [
                sg.Text('Estimated BPM: '),
                sg.Text(' ' * 5, key='-SONGBPM-'),
                sg.Button('/ 2', key='-DIVIDE2-'),
                sg.Button('* 2', key='-TIMES2-'),
            ],
        ], key='-SONGCONTROLS-', visible=False)]
    ]
    export_column = [
        # TODO: Unvisible this when functionality works.
        [sg.Button('Play', key='-PLAYPAUSE-', visible=False, enable_events=True)],
        [
            sg.FileSaveAs('Export And Save', file_types=(('Video File', '*.mp4'), ('All Files', '*.*')), default_extension='mp4'),
            sg.Input(key='-EXPORTFILENAME-', visible=False, enable_events=True)],
    ]
    layout = [
        [
            sg.Column(image_preview_column),
            sg.Column(song_preview_column),
            sg.Column(export_column),
        ]
    ]

    return sg.Window('Window Title', layout, finalize=True)

def load_song(values, bpm_elem, canvas):
    """
    Args:
        values (Dict): The PySimpleGUI event values.
        bpm_elem (Text): A Text Element to update with bpm.
        canvas (Canvas): A Canvas object to redraw.
    """
    try:
        amplitude, sample_rate = librosa.load(values['-SONGFILENAME-'])
    except NoBackendError:
        # Failed to load the file.
        return False
    tempo, _beats = librosa.beat.beat_track(y=amplitude, sr=sample_rate)
    bpm_elem.update(int(tempo))

    # By default x axis will be the array index. Use that value and
    # divide by sample rate to get time.
    time = np.linspace(0, len(amplitude) / sample_rate, num=len(amplitude))

    plt.clf()
    plt.plot(time, amplitude)
    update_plot_verticals(values)
    canvas.draw()
    return True

def into_file_bytes(gif_io):
    bio = io.BytesIO()
    gif_io.save(bio, format='GIF')
    return bio.getvalue()


def estimate_gif_bpm(file, beat_frame):
    # I don't want to figure out how to read metadata via PIL so
    # using imageio.
    reader = imageio.get_reader(file)
    count = 0
    time_until_beat_ms = 0
    for frame in reader:
        # Pretty sure the standard says this is in hundredths of seconds, but
        # I've found it to be ms. /shrug
        time_until_beat_ms += frame.meta.duration
        count += 1
        if count == beat_frame:
            break

    # Number of ms in a minute / ms per beat.
    return int(60.0 * 1000.0 / time_until_beat_ms)


def time_str_to_seconds(time_str):
    """
    Returns:
        None: If time_str is unparseable.
        float: Number of seconds the time string represents.
    """
    try:
        duration = datetime.strptime(time_str, '%H:%M:%S.%f')
        return duration.hour * 60.0 * 60.0 + duration.minute * 60.0 + duration.second + duration.microsecond / 1000000.0
    except ValueError:
        return -1.0



_start_line = None
_end_line = None
def update_plot_verticals(values):
    """
    Args:
        values (Dict): The PySimpleGUI event values.
    """
    workload = {
        ('_start_line', values['-STARTTIME-']),
        ('_end_line', values['-ENDTIME-']),
    }
    for line_var, value in workload:
        if not globals()[line_var]:
            globals()[line_var] = plt.axvline(x=-1.0, c='r')

        line_plot = globals()[line_var]
        plot_index = time_str_to_seconds(value)
        line_plot.set_xdata(plot_index or 0.0)
        line_plot.set_visible(plot_index is not None)

def setup_matplotlib(canvas):
    plt.figure()
    fig = plt.gcf()
    ax = plt.gca()
    ax.axes.xaxis.set_visible(False)
    ax.axes.yaxis.set_visible(False)
    figure_canvas_agg = FigureCanvasTkAgg(fig, canvas)
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

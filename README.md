# GIFonbeat
Merges a GIF with an audio file attempting to match beats.

# Usage
gifonbeat has a "friendly" graphical interface where one can define a beat
marker in a GIF, and start and stop times of an audio file.
Your GIF is assumed to start on a "beat".

## CLI
See --help for usage.

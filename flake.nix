{
    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs";
        utils.url = "github:numtide/flake-utils";
        poetry2nix = {
          url = "github:nix-community/poetry2nix";
          inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = { self, nixpkgs, utils, poetry2nix }:
    {
      # Nixpkgs overlay providing the application
      overlay = nixpkgs.lib.composeManyExtensions [
        poetry2nix.overlay
        (final: prev: {
          gifonbeat = prev.poetry2nix.mkPoetryApplication {
            projectDir = ./.;
          };
        })
      ];
    } // (utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ self.overlay ];
        };
      in
      {
        devShell = let
          gifonbeat_editable = pkgs.poetry2nix.mkPoetryEditablePackage {
            projectDir = ./.;
            editablePackageSources = {
              gifonbeat_editable = ./gifonbeat;
            };
          };
        in pkgs.mkShell {
          buildInputs = with pkgs; [
            gifonbeat_editable
            python39Packages.tkinter
            python39Packages.librosa
            python39Packages.soundfile

            llvmPackages_10.libllvm
            libsndfile
          ];
        };
        apps = {
          gifonbeat = pkgs.gifonbeat;
        };

        defaultApp = pkgs.gifonbeat;
      }));
}
